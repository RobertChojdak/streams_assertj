package streams;

import java.util.ArrayList;
import java.util.List;

public class User {
	
	public enum Role { admin, user, guest }
	
	private String userName;
	private String permission;
	private String address;
	private Role role;
	private List<String> addresses = new ArrayList();
	private Person person;
	
	public User(String userName, String permission, String address, Role role, String firstName, String lastName, int age) {
		this.userName = userName;
		this.permission = permission;
		addresses.add(address);
		this.role = role;
		this.person = new Person(firstName, lastName, age);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPermissionName() {
		return permission;
	}

	public void setPermissionName(String permission) {
		this.permission = permission;
	}

	public List<String> getAddress() {
		return addresses;
	}

	public void addAddress(String address) {
		this.addresses.add(address);
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getFirstName() {
		return person.getFirstName();
	}
	
	public String getLastName() {
		return person.getLastName();
	}
	
	public int getAge() {
		return person.getAge();
	}
	
	public String toString() {
		return person.getFirstName() + ", " + person.getLastName() + ", " + userName;
	}
}
