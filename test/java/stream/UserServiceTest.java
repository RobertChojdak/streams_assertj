package stream;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.junit.Test;

import streams.*;

public class UserServiceTest {

	UserService userService = new UserService();

	User personDetails1 = new User("pioter", "user", "http://test.org", User.Role.user, "Piotr", "Kowalski", 22);
	User personDetails2 = new User("adrian", "guest", "http://nowa.strona.org", User.Role.guest, "Adrian", "Pawlak", 42);
	User personDetails3 = new User("maryjan", "admin", "http://strona.startowa.pl", User.Role.admin, "Marian", "Lipko", 38);
	User personDetails4 = new User("tadzik", "admin", "http://drugie.podejscie.com.pl", User.Role.admin, "Tadeusz", "Barom", 23);
	User personDetails5 = new User("franek", "user", "http://strona.startowa.pl", User.Role.user, "Franciszek", "Paprota", 54);
	User personDetails6 = new User("antek", "admin", "http://strona.startowa.pl", User.Role.admin, "Antoni", "Marzec", 27);
	
	List<User> users = Arrays.asList(personDetails1, personDetails2, personDetails3, personDetails4, personDetails5, personDetails6);
	List<User> usersEmptyList = new ArrayList();
	List<User> userNotInitialized;
 	
	@Test
	public void findUserWithLongestUsername_True () {
		assertThat(userService.findUserWithLongestUsername(users)).isNotNull().isEqualTo(personDetails3);
	}
	@Test
	public void findUserWithLongestUsername_False () {
		assertThat(userService.findUserWithLongestUsername(users)).isNotNull().isNotEqualTo(personDetails1);
	}
	
	@Test
	public void findOldestPerson_NotEmptyList_True() {
		assertThat(userService.findOldestPerson(users)).isNotNull().isEqualTo(personDetails5);
	}
	@Test
	public void findOldestPerson_NotEmptyList_False() {
		assertThat(userService.findOldestPerson(users)).isNotNull().isNotEqualTo(personDetails1);
	}
	
	@Test
	public void partitionUserByUnderAndOver18_ContainKey_True() {
		assertThat(userService.partitionUserByUnderAndOver18(users).containsKey("true"));
	}
	@Test
	public void partitionUserByUnderAndOver18_ContainKey_False() {
		assertThat(userService.partitionUserByUnderAndOver18(users).containsKey("jeden"));
	}
	@Test
	public void partitionUserByUnderAndOver18_ContainValue_True() {
		assertThat(userService.partitionUserByUnderAndOver18(users).containsValue(users));
	}
	@Test
	public void partitionUserByUnderAndOver18_ContainValue_False() {
		assertThat(userService.partitionUserByUnderAndOver18(users).containsValue(userNotInitialized));
	}
	@Test
	public void partitionUserByUnderAndOver18_KeyWithNullValue_True() {
		assertThat(userService.partitionUserByUnderAndOver18(users).isEmpty());
	}
	@Test
	public void partitionUserByUnderAndOver18_OnlyKeys_True() {
		assertThat(userService.partitionUserByUnderAndOver18(usersEmptyList).values().isEmpty());
	}
}
