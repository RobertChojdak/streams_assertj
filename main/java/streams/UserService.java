package streams;

import java.util.*;
import java.util.stream.Collectors;




public class UserService {
	
	public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getAddress().size() > 1)
		.collect(Collectors.toList());
	}
	
	public static User findOldestPerson(List<User> users) {
		return users.stream()
		.max(Comparator.comparing(User::getAge))
		.get();
	}
	
	public static User findUserWithLongestUsername(List<User> users) {
		return users.stream()
		.reduce((personDetails1, personDetails2)->personDetails1.getUserName().length() >= personDetails2.getUserName().length() ? personDetails1 : personDetails2)
		.get();
	}
	
	public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getAge() > 18)
		.map(personDetails -> personDetails.getFirstName().concat(", ").concat(personDetails.getLastName()))
		.collect(Collectors.joining(", "));
	};
	
	public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getFirstName().toUpperCase().startsWith("A"))
		.map(personDetails -> personDetails.getPermissionName())
		.sorted()
		.collect(Collectors.toList());
	}
	
	public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
		users.stream()
		.filter(personDetails -> personDetails.getLastName().toUpperCase().startsWith("S"))
		.map(personDetails -> personDetails.getPermissionName().toUpperCase())
		.forEach(System.out::println);
	}

	public static Map<User.Role, List<User>> groupUsersByRole(List<User> users) {
		return users.stream()
		.collect(Collectors.groupingBy(User::getRole));
	};
	
	public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
		return users.stream()
		.collect(Collectors.partitioningBy(personDetails -> personDetails.getAge() >= 18))
		;
	}
}
